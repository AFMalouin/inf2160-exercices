# Laboratoire 6: Foncteurs applicatifs

## 1 - Images

L'objectif de cette question est de proposer l'implémentation d'un module
`Image.hs` qui fournit des types et des fonctions permettant de manipuler des
images 2D simplifiées. En particulier, il s'agit de bien exploiter les classes
prédéfinies `Functor` et `Applicative`.

Complétez l'implémentation des fonctions qui se trouvent dans le fichier
[Image.hs](Image.hs).

## 2 - Arbres

Considérez le type suivant, qui implémente une structure arborescente dans
laquelle chaque noeud peut avoir un nombre quelconque d'enfants. Aussi, on
distingue les sommets qui sont des feuilles à l'aide du constructeur `Leaf`.
```haskell
data Tree a = Empty | Leaf a | Branch a [Tree a]
    deriving Show
```

Proposez une définition de la classe `Applicative` pour le constructeur de type
`Tree` qui a le comportement suivant:
```haskell
>>> let tree1 = Branch 2 [Leaf 2, Leaf 5]
>>> let tree2 = Branch 1 [Leaf 2, Leaf 3, Leaf 7]
>>> (+) <$> tree1 <*> tree2
Branch 3 [Leaf 4,Leaf 5,Leaf 9,Leaf 7,Leaf 8,Leaf 12]

>>> let tree3 = Branch 3 [Leaf 2, Leaf 5, Branch 7 [Leaf 5, Leaf 9]]
>>> let tree4 = Branch 2 [Leaf 2, Branch 5 [Leaf 1], Leaf 7, Branch 7 [Leaf 5, Leaf 9]]
>>> (+) <$> tree1 <*> tree2
Branch 5 [Leaf 4,Leaf 7,Leaf 9,Leaf 9,Leaf 7,Leaf 10,Leaf 12,Leaf 12,Leaf 9,Branch 12 [Leaf 6,Leaf 10],Leaf 14,Branch 14 [Leaf 10,Leaf 14,Leaf 14,Leaf 18]]
```
Autrement dit, lorsqu'on rencontre un embranchement, on prend toutes les
combinaisons possibles des enfants de chaque côté.

Croyez-vous que cette implémentation respecte les quatre lois de la classe
`Applicative`? Si oui, expliquez brièvement pourquoi. Sinon, dites quelles lois
ne sont pas respectées en fournissant un contre-exemple pour chacune d'elles.
