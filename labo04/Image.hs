module Fruits where

-- | Une couleur représentée par son niveau de rouge, vert et bleu
data Couleur = RGB Int Int Int

-- | Une image
data Image = Image {
    hauteur :: Int,
    largeur :: Int,
    pixels :: [[Couleur]]
}

-- | Indique si la hauteur et la largeur de l'image sont cohérentes avec la
-- matrice de pixels
estValide :: Image -> Bool
estValide = error "À compléter"

-- | Retourne une image de dimensions données dont tous les pixels sont de même
-- couleur
imageUnicolore :: Int     -- Hauteur
               -> Int     -- Largeur
               -> Couleur -- La couleur
               -> Image   -- L'image résultante
imageUnicolore = error "À compléter"

-- | Retourne une image de dimensions données en alternant les couleurs données
-- comme dans un échiquier.
echiquier :: Int     -- Hauteur
          -> Int     -- Largeur
          -> Couleur -- Première couleur
          -> Couleur -- Deuxième couleur
          -> Image   -- L'image résultante
echiquier = error "À compléter"
