module Arbre where

-- | Un arbre binaire de recherche classique
data ABR a = ABRVide | Noeud a (ABR a) (ABR a)
    deriving (Show)

-- | Insertion
inserer :: Ord a => a -> ABR a -> ABR a
inserer x ABRVide = Noeud x ABRVide ABRVide
inserer x (Noeud y g d)
    | x == y = Noeud x g d
    | x < y  = Noeud y (inserer x g) d
    | x > y  = Noeud y g (inserer x d)

-- | Insertions multiples
insererPlusieurs :: Ord a => ABR a -> [a] -> ABR a
insererPlusieurs = foldl (flip inserer)

-- | Appartenance
appartient :: Ord a => a -> ABR a -> Bool
appartient _ ABRVide = False
appartient x (Noeud y g d)
    | x == y = True
    | x < y  = appartient x g
    | x > y  = appartient x d

-- | Nombre de noeuds
--
-- Note: Un arbre vide n'est pas un noeud.
nbNoeuds :: ABR a -> Int
nbNoeuds = error "À compléter"

-- | Nombre de feuilles
--
-- Note: Une feuille est un noeud dont les deux sous-arbres sont vides.
nbFeuilles :: ABR a -> Int
nbFeuilles = error "À compléter"

-- | Hauteur
--
-- Note: Un arbre vide est de hauteur 0, alors qu'une feuille est de hauteur 1.
hauteur :: ABR a -> Int
hauteur = error "À compléter"

-- | Est-ce que l'arbre est équilibré?
--
-- Un arbre est équilibré si la différence de hauteur entre *chacun* de ses
-- sous-arbres gauche et droite diffère d'au plus 1.
estEquilibre :: ABR a -> Bool
estEquilibre = error "À compléter"
